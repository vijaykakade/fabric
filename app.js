'use strict';

let canvasGenerator = document.getElementById('generatecanvas');
let shapeGenarator = document.getElementById('generateshape');
let body = document.getElementById('canvas-wrapper');
let fabcan;

const circle = new fabric.Circle({
    left: 100, top: 50, radius: 30, fill: 'green'
});
const triangle = new fabric.Triangle({
    left: 100, top: 50, fill: 'blue', width: 50, height: 50
});
const rect = new fabric.Rect({
    left: 110, top: 50, fill: '#f0f', width: 50, height: 50
})

canvasGenerator.addEventListener('click', onClickGenerateCanvas);
shapeGenarator.addEventListener('click', onClickGenerateShape);


function onClickGenerateCanvas() {

    let canvas = document.createElement('canvas');
    canvas.id = makeid();
    canvas.classList.add('custom-canvas');
    body.appendChild(canvas);

    let idSelect = document.getElementById('canvasid');
    idSelect.options[idSelect.options.length] = new Option('Canvas_' + canvas.id, canvas.id);

}

function onClickGenerateShape() {
    let canvasid = document.getElementById('canvasid').value;
    let shape = document.getElementById('shape').value;

    fabcan = new fabric.Canvas(canvasid);
    
    switch (shape) {
        case 'circle':
            fabcan.add(circle);
            break;
        case 'triangle':
            fabcan.add(triangle);
            break;
        case 'rectangle':
            fabcan.add(triangle);
            break;
    }
    fabcan.item(0);
    fabcan.getObjects();
    fabcan.remove(shape);
}


function makeid() {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}